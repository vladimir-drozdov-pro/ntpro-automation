import datetime

from constants import DATE_TIME_MINUTES_FORMAT


def set_default_range_last_month():
    now = datetime.datetime.now()

    year = now.year
    month = now.month
    first_month = datetime.datetime(year=year, month=month, day=1)
    if month > 1:
        month -= 1
    else:
        year -= 1
        month = 1
    first_prev_month = datetime.datetime(year=year, month=month, day=1)

    return {
        'start': first_prev_month.strftime(DATE_TIME_MINUTES_FORMAT),
        'end': (first_month - datetime.timedelta(minutes=1)).strftime(DATE_TIME_MINUTES_FORMAT)
    }
