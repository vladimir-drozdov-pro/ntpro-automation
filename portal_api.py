import json
import urllib.parse

from auth import qt_auth_params, HTTPClientConnection, hasher
from constants import NTPRO_PORTAL_HOST, NTPRO_PORTAL_API_USER, NTPRO_PORTAL_API_PASSWORD, NTPRO_PORTAL_REPORT_URL


class PortalApi:
    def get(self, from_date, to_date):
        test_login = {'username': NTPRO_PORTAL_API_USER, 'password_hash': hasher(NTPRO_PORTAL_API_PASSWORD)}
        auth_params = qt_auth_params(test_login)
        auth_params['datasource'] = 1
        auth_params['trade_time__startDate'] = from_date
        auth_params['trade_time__endDate'] = to_date
        params = urllib.parse.urlencode(auth_params)
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        data = {}
        with HTTPClientConnection(NTPRO_PORTAL_HOST, secure=True) as conn:
            conn.request("GET", f"{NTPRO_PORTAL_REPORT_URL}?{params}", None, headers)
            response = conn.getresponse()
            if response.status != 200:
                print(f"Bad response status {response.status}")
            else:
                print('response status 200 OK')
                data_response = response.read()
                response_json = json.loads(data_response)
                data = response_json['dataTable']['trades_pl_analysis_by_client']['fetchedRows']

        return data
