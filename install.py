#!/usr/bin/env python3


import runpy
import os
import shutil
import subprocess


PROJECT_ROOT = os.getcwd()
venv_path = os.path.join(PROJECT_ROOT, 'venv')
venv_bin_path = os.path.join(venv_path, 'bin')
pip_path = os.path.join(venv_bin_path, 'pip3')

skip_venv = False
if os.path.exists(venv_path):
    print(f'found existing virtual env at {venv_path}. use it?')
    answer = input('yes/no? ')
    if answer == 'yes':
        skip_venv = True
    else:
        shutil.rmtree(venv_path)

if not skip_venv:
    print(f'installing virtualenv {venv_path}')
    subprocess.check_call(['python3', '-m', 'virtualenv', venv_path, '-p', 'python3'])
else:
    print(f'using virtual env {venv_path}')

requirements = os.path.join(PROJECT_ROOT, 'requirements.txt')
print(f'installing modules from {requirements}')
subprocess.check_call([pip_path, 'install', '-r', requirements])

activate_this_path = os.path.join(venv_bin_path, "activate_this.py")
print(f"going to activate {activate_this_path}")
runpy.run_path(activate_this_path, {"__file__": activate_this_path})
