import asyncio
import sys

import logging

from portal_api import PortalApi
from utils import set_default_range_last_month
from ws_api import WSApi

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                    format="[%(asctime)s: %(levelname)s] %(message)s")

portal_api = PortalApi()

change_group = []

date_range = set_default_range_last_month()

print('starting request nt-pro...')

# groups = portal_api.get(date_range['start'], date_range['end'])

# print(f'get data: {groups}')

# for group in groups:
#     if is_bad_group(group):
#         change_group.append((group['account_id'], group.id))
#     # ...

ws_api = WSApi(get_accounts_flag=True)

accounts = asyncio.run(ws_api.get_accounts())

for account_id, executor_group_id in change_group:
    ws_api = WSApi(update_accounts_flag=True, accounts_cache=accounts)
    asyncio.run(ws_api.update_executor_group(account_id, executor_group_id))
