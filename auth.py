import base64
import http.client
import sha3
import time


def hasher(password):
    k = sha3.keccak_512()
    k.update(password.encode('utf8'))
    my_hash = base64.standard_b64encode(k.digest())
    return my_hash.decode('utf8')


class QTAuthorization:
    SIGN_AUTH_SALT = "^NTProgress$"
    PARAM_TIMESTAMP = "_trequest_timestamp"
    PARAM_USERNAME = "_trequest_username"
    PARAM_SIGN = "_trequest_sign"

    @staticmethod
    def get_sign(timestamp, username, password_hash):
        sign_source = timestamp + QTAuthorization.SIGN_AUTH_SALT + username + password_hash
        return hasher(sign_source)


def qt_auth_params(login, timestamp_dt=None):
    if timestamp_dt is None:
        timestamp = int(time.time())
    else:
        timestamp = timestamp_dt.timestamp()

    timestamp = str(timestamp)

    sign = QTAuthorization.get_sign(timestamp, login['username'], login['password_hash'])

    return {
        QTAuthorization.PARAM_SIGN: sign,
        QTAuthorization.PARAM_USERNAME: login['username'],
        QTAuthorization.PARAM_TIMESTAMP: timestamp
    }


class HTTPClientConnection(object):
    def __init__(self, host, secure=False):
        self.secure = secure
        self.host = host
        self.conn = None

    def enter(self):
        if self.secure:
            self.conn = http.client.HTTPSConnection(self.host)
        else:
            self.conn = http.client.HTTPConnection(self.host)
        return self.conn

    def __enter__(self):
        return self.enter()

    # noinspection PyUnusedLocal
    def exit(self, exc_type, exc_val, exc_tb):
        self.conn.close()

    def __exit__(self, exc_type, exc_val, exc_tb):
        return self.exit(exc_type, exc_val, exc_tb)
