import asyncio
from logging import getLogger

import json
import websockets
from typing import Optional, AsyncIterable, cast

from ws_api_messages import NTProMessage, NTProMessageType, NTProDataMessage, NTProRequestType
from constants import NTPRO_PORTAL_HOST, NTPRO_WS_API_PASSWORD, NTPRO_WS_API_USER, PUBLIC_WS_API_MAJOR_VERSION, \
    PUBLIC_WS_API_MINOR_VERSION


class WSApi:
    def __init__(self, get_accounts_flag: bool = False, update_accounts_flag: bool = False, accounts_cache: {} = None):
        self.send_waiting_messages = []
        self.websocket: Optional[websockets.WebSocketClientProtocol] = None
        self.logger = getLogger(__name__)
        self.accounts_cache = accounts_cache
        self.request_id: int = 1
        self.get_accounts_flag: bool = get_accounts_flag
        self.update_account_flag: bool = update_accounts_flag
        self.account_id: int
        self.executor_group_id: int

    async def auth(self):
        url = f"wss://{NTPRO_PORTAL_HOST}/ws/ntpro_proxy/"

        async with websockets.connect(url, max_size=None, ping_interval=None) as websocket:
            self.logger.info(f"Connected to: {url}")
            self.websocket = websocket

            self.send_waiting_messages.append(NTProDataMessage(
                RequestId=1,
                OriginalRequestId=0,
                RequestType=NTProRequestType.Hello,
                Msg={
                    "VersionMajor": PUBLIC_WS_API_MAJOR_VERSION,
                    "VersionMinor": PUBLIC_WS_API_MINOR_VERSION,
                }
            ))

            done, pending = await asyncio.wait([self.consumer(), self.producer()],
                                               return_when=asyncio.FIRST_COMPLETED)
            for future in pending:
                future.cancel()

            for future in done:
                if future.cancelled():
                    self.logger.info(f"{self.__class__.__name__} stopped because of cancelled future {future}")
                elif future.done():
                    raise asyncio.CancelledError()
                else:
                    exception = future.exception()
                    if exception:
                        self.logger.exception(f"{self.__class__.__name__} stopped because of exception in future {future}")
                        raise exception
                    else:
                        raise asyncio.CancelledError()

            raise asyncio.TimeoutError()

    async def consumer(self):
        while True:
            if not self.websocket:
                await asyncio.sleep(0.1)
                continue

            async for data in cast(AsyncIterable, self.websocket):
                message = await self.consumer_parser(data)
                if message.Type == NTProMessageType.PING_REQUEST:
                    self.send_waiting_messages.append(NTProMessage(Type=NTProMessageType.PING_REPLY,
                                                                   Msg=message.Msg))
                elif message.Type == NTProMessageType.DATA_MESSAGE:
                    # requests sequence
                    if message.RequestType == NTProRequestType.Environment:
                        self.request_id += 1
                        self.send_waiting_messages.append(NTProDataMessage(
                            RequestId=self.request_id,
                            OriginalRequestId=self.request_id,
                            RequestType=NTProRequestType.LoginRequest,
                            Msg={"UserName": NTPRO_WS_API_USER,
                                 "Password": NTPRO_WS_API_PASSWORD,
                                 'ClientAppType': 1}
                        ))

                    if message.RequestType == NTProRequestType.LoginReply:
                        self.request_id += 1
                        if self.get_accounts_flag:
                            self.send_waiting_messages.append(NTProDataMessage(
                                RequestId=self.request_id,
                                OriginalRequestId=self.request_id,
                                RequestType=NTProRequestType.SelectUserRoleRequest,
                                Msg={'SelectedRole': 2}
                            ))
                        if self.update_account_flag:
                            self.request_id += 1
                            # todo отладить отправку запроса ChangeAccountRequest (поле Msg и RequestId)
                            account = self.accounts_cache.get(self.account_id)
                            account['ExecutorGroupId'] = self.executor_group_id
                            update_executor_group_message = NTProDataMessage(
                                RequestId=self.request_id,
                                OriginalRequestId=self.request_id,
                                RequestType=NTProRequestType.ChangeAccountRequest,
                                Msg=account
                            )
                            self.send_waiting_messages.append(update_executor_group_message)

                    if message.RequestType == NTProRequestType.CommandReply:
                        self.request_id += 1
                        self.send_waiting_messages.append(NTProDataMessage(
                            RequestId=self.request_id,
                            OriginalRequestId=self.request_id,
                            RequestType=NTProRequestType.RequestAccountStates,
                            Msg={}
                        ))

                    if message.RequestType == NTProRequestType.AccountStatesUpdate:
                        self.save_accounts_cache(message.Msg['AccountStates'])
                        return

    async def consumer_parser(self, data: str) -> Optional[NTProMessage]:
        message_json = json.loads(data)
        message = NTProMessage.from_json(message_json)

        self.logger.info(f"< {message_json}")

        return message

    async def send(self, message: NTProMessage):
        message_json = message.to_json()
        message_data = json.dumps(message_json)
        self.logger.info(f"> {message_data}")

        await self.websocket.send(message_data)

    async def producer(self):
        while True:
            if self.send_waiting_messages:
                message = self.send_waiting_messages.pop(0)

                try:
                    await self.send(message)
                except websockets.ConnectionClosed:
                    return

                continue

            await asyncio.sleep(0.1)

    async def get_accounts(self):
        await self.auth()

        return self.accounts_cache

    async def update_executor_group(self, account_id, executor_group_id):
        if not self.accounts_cache:
            await self.get_accounts_flag()

        self.account_id = account_id
        self.executor_group_id = executor_group_id

        await self.auth()

    # todo отладить преобразование модели AccountInfo из AccountStatesUpdate в Account
    def save_accounts_cache(self, account_states):
        for account_state in account_states:
            account = account_state.key
            account_id: int = account.Id
            self.accounts_cache[account_id] = account
